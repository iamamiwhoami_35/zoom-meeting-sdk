import React, { useState } from "react";
import Client from "./Client";
import "./App.css";
import Component from "./Component";

const CLIENT = "CLIENT";
const COMPONENT = "COMPONENT";

const App = () => {
  const [type, setType] = useState("");
  var signatureEndpoint =
    "https://2kkgf3nk2mcuplc427i6pxkmcu0xapim.lambda-url.ap-south-1.on.aws/";
  var sdkKey = "Jj69auQxH0dkjtDh41IJpZxwDaV6D5r3dsGD";
  var meetingNumber = "99977815117";
  var role = 0;
  var leaveUrl = "http://localhost:3000";
  var userName = "Pardhu";
  var userEmail = "";
  var passWord = "94m1VY";
  var registrantToken = "";
  return (
    <div>
      <div>
        <button
          style={{ margin: "16px", color: "blue" }}
          onClick={() => {
            setType(CLIENT);
          }}
        >
          Client View
        </button>
        <button
          style={{ margin: "16px", color: "blue" }}
          onClick={() => {
            setType(COMPONENT);
          }}
        >
          Component View
        </button>
      </div>
      <div>
        {type === CLIENT && (
          <Client
            signatureEndpoint={signatureEndpoint}
            userEmail={userEmail}
            userName={userName}
            passWord={passWord}
            meetingNumber={meetingNumber}
            role={role}
            leaveUrl={leaveUrl}
            sdkKey={sdkKey}
            registrantToken={registrantToken}
          />
        )}
        {type === COMPONENT && (
          <Component
            signatureEndpoint={signatureEndpoint}
            userEmail={userEmail}
            userName={userName}
            passWord={passWord}
            meetingNumber={meetingNumber}
            role={role}
            leaveUrl={leaveUrl}
            sdkKey={sdkKey}
            registrantToken={registrantToken}
          />
        )}
      </div>
    </div>
  );
};

export default App;
