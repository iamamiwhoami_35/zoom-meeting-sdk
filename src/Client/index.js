import React from "react";
import "../App.css";
import { ZoomMtg } from "@zoomus/websdk";

ZoomMtg.setZoomJSLib("https://source.zoom.us/2.3.5/lib", "/av");

ZoomMtg.preLoadWasm();
ZoomMtg.prepareWebSDK();
ZoomMtg.i18n.load("en-US");
ZoomMtg.i18n.reload("en-US");

const Client = ({
  signatureEndpoint,
  meetingNumber,
  passWord,
  userEmail,
  userName,
  role,
  sdkKey,
  leaveUrl,
  registrantToken,
}) => {
  const getSignature = (e) => {
    e.preventDefault();

    fetch(signatureEndpoint, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        meetingID: meetingNumber,
      }),
    })
      .then((res) => res.json())
      .then((response) => {
        console.log(response.signature);
        startMeeting(response.signature);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const startMeeting = (signature) => {
    document.getElementById("zmmtg-root").style.display = "block";

    ZoomMtg.init({
      audioPanelAlwaysOpen: false,
      showMeetingHeader: false,
      leaveUrl: leaveUrl,
      screenShare: false,
      isSupportChat: false,
      disableCORP: true,
      disableJoinAudio: true,
      disableCallOut: true,
      disableVoIP: true,
      disablePreview: true,
      videoHeader: true,
      showPureSharingContent: false,
      disableRecord: true,
      isSupportNonverbal: false,
      isSupportAV: false,
      success: (success) => {
        console.log(success);

        ZoomMtg.join({
          signature: signature,
          meetingNumber: meetingNumber,
          userName: userName,
          sdkKey: sdkKey,
          userEmail: userEmail,
          passWord: passWord,
          tk: registrantToken,
          success: (success) => {
            console.log(success);
          },
          error: (error) => {
            console.log(error);
          },
        });
      },
      error: (error) => {
        console.log(error);
      },
    });
  };

  return (
    <div className="App">
      <main>
        <h1>Zoom Meeting SDK Client View</h1>

        <button onClick={getSignature}>Join Meeting</button>
      </main>
    </div>
  );
};

export default Client;
