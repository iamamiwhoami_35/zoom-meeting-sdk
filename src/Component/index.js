import React from "react";
import ZoomMtgEmbedded from "@zoomus/websdk/embedded";
import "../App.css";

const Component = ({
  signatureEndpoint,
  meetingNumber,
  passWord,
  userEmail,
  userName,
  role,
  sdkKey,
  leaveUrl,
  registrantToken,
}) => {
  const client = ZoomMtgEmbedded.createClient();

  const getSignature = (e) => {
    e.preventDefault();

    fetch(signatureEndpoint, {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        meetingID: meetingNumber,
        role: role,
      }),
    })
      .then((res) => res.json())
      .then((response) => {
        console.log(response, "response from cloud");
        startMeeting(response.signature);
      })
      .catch((error) => {
        console.error(error, "error from cloud");
      });
  };

  const startMeeting = (signature) => {
    let meetingSDKElement = document.getElementById("meetingSDKElement");

    client.init({
      debug: true,
      zoomAppRoot: meetingSDKElement,
      language: "en-US",

      customize: {
        meetingInfo: [
          "topic",
          "host",
          "mn",
          "pwd",
          "telPwd",
          "invite",
          "dc",
          "enctype",
        ],
        toolbar: {
          buttons: [
            {
              text: "Custom Button",
              className: "CustomButton",
              onClick: () => {
                console.log("custom button");
              },
            },
          ],
        },
      },
    });

    client.join(
      {
        sdkKey: sdkKey,
        signature: signature,
        meetingNumber: meetingNumber,
        password: passWord,
        userName: userName,
        userEmail: userEmail,
      },
      leaveUrl
    );
  };

  return (
    <main>
      <h1>Zoom Meeting SDK Component View</h1>

      <button onClick={getSignature}>Join Meeting</button>
      <div id="meetingSDKElement"></div>
    </main>
  );
};

export default Component;
